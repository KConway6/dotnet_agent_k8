﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using dotnet_backend.Models;
using System.Web.Http.Cors;

namespace dotnet_backend.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Route("api/[controller]")]
    [ApiController]
    public class ThingsController : ControllerBase
    {
        private readonly ThingContext _context;

        public ThingsController(ThingContext context)
        {
            _context = context;
        }

        // GET: api/Things
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Thing>>> GetThingItems()
        {
            return await _context.ThingItems.ToListAsync();
        }

        // GET: api/Things/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Thing>> GetThing(int id)
        {
            var thing = await _context.ThingItems.FindAsync(id);

            if (thing == null)
            {
                return NotFound();
            }

            return thing;
        }

        // PUT: api/Things/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutThing(int id, Thing thing)
        {
            if (id != thing.id)
            {
                return BadRequest();
            }

            _context.Entry(thing).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ThingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Things
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Thing>> PostThing(Thing thing)
        {
            _context.ThingItems.Add(thing);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Thing), new { id = thing.id }, thing);
            // return CreatedAtAction("GetThing", new { id = thing.id }, thing);
        }

        // DELETE: api/Things/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Thing>> DeleteThing(int id)
        {
            var thing = await _context.ThingItems.FindAsync(id);
            if (thing == null)
            {
                return NotFound();
            }

            _context.ThingItems.Remove(thing);
            await _context.SaveChangesAsync();

            return thing;
        }

        private bool ThingExists(int id)
        {
            return _context.ThingItems.Any(e => e.id == id);
        }
    }
}
