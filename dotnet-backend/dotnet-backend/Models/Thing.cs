﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_backend.Models
{
    public class Thing
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
