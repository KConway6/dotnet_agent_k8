﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace dotnet_backend.Models
{
    public class ThingContext : DbContext
    {
        public ThingContext(DbContextOptions<ThingContext> options)
            : base(options)
        {

        }

        public DbSet<Thing> ThingItems { get; set; }
    }
}
